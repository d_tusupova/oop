﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface ISquareCalculatatable
    {
       
        double Area();
    }

    class Rectangle : ISquareCalculatatable
    {
        public override string ToString()
        {
            return string.Format("height: {0} width: {1} area:{2}", height, width, Area());
        }
        public override bool Equals(object obj)//нужно для того чтобы компаратор сравнил
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()// возвращает число которое определяет его айди
        {
            return base.GetHashCode();
        }
        private double height;
        private double width;

        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("negative or zero value is passed");
                }
                width = value;
            }
        }
        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("negative or zero value is passed");
                }
                height = value;
            }
        }
        /// <summary>
        /// function which return Area of Rectangle
        /// </summary>
        /// <returns></returns>
        public double Area()
        {
            return height * width;
        }
        /* class Square : IComparable
         {
             public double Area()
             {
                 return base.;
             }
             public int CompareTo(object obj)
             {
                 throw new NotImplementedException();
             }*/
        class Triangle : ISquareCalculatatable
        {
            public override string ToString()
            {
                return string.Format("height: {0} base: {1} area:{2}", height, a, Area());
            }

            private double height;
            private double a;

            public double A
            {
                get
                {
                    return a;
                }
                set
                {
                    if (value <= 0)
                    {
                        throw new Exception("negative or zero value is passed");
                    }
                    a = value;
                }
            }
            public double Height
            {
                get
                {
                    return height;
                }
                set
                {
                    if (value <= 0)
                    {
                        throw new Exception("negative or zero value is passed");
                    }
                    height = value;
                }
            }
            /// <summary>
            /// function which return area of triangle
            /// </summary>
            /// <returns></returns>
            public double Area()
            {
                return 0.5 * height * a;
            }

            class Circle : ISquareCalculatatable
            {
                public override string ToString()
                {
                    return string.Format("radius: {0} area:{1}", radius, Area());
                }

                private double radius;



                public double Radius
                {
                    get
                    {
                        return radius;
                    }
                    set
                    {
                        if (value <= 0)
                        {
                            throw new Exception("negative or zero value is passed");
                        }
                        radius = value;
                    }
                }
                /// <summary>
                /// function which return area of circle
                /// </summary>
                /// <returns></returns>
                public double Area()
                {
                    return 3.14 * radius * radius;
                }
            }

            class Program
            {
                static void Main(string[] args)
                {
                    try
                    {
                        Rectangle r = new Rectangle();
                        r.Height = 10;
                        r.Width = 10;
                        //Console.WriteLine(r.Area());
                        Console.WriteLine(r.ToString());
                        Triangle t = new Triangle();
                        t.Height = 15;
                        t.A = 2;
                       // Console.WriteLine(t.Area());
                        Console.WriteLine(t.ToString());
                        Circle c = new Circle();
                        c.Radius = 10;
                        //Console.WriteLine(c.Area());
                        Console.WriteLine(c.ToString());
                        Console.ReadLine();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                    }
                }
            }
        }
    }
}
